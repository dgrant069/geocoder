class IpAddress < ActiveRecord::Base
  attr_accessible :latitude, :location, :longitude

  geocoded_by :location,
              :latitude => :latitude, :longitude => :longitude
  after_validation :geocode
end
